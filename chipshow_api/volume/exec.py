import asyncio
from dataclasses import dataclass
from chipshow_api.logger import logger


@dataclass
class VolumeProcessState:
    stdout: bytes
    stderr: bytes


class Volume(object):
    def __init__(self, mixer, vol_up, vol_down):
        self.mixer = mixer
        self.vol_up = vol_up
        self.vol_down = vol_down

    async def increase_volume(self) -> VolumeProcessState:
        proc = await asyncio.create_subprocess_exec(
            self.mixer,
            *self.vol_up,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        stdout, stderr = await proc.communicate()
        return VolumeProcessState(stdout, stderr)

    async def decrease_volume(self) -> VolumeProcessState:
        proc = await asyncio.create_subprocess_exec(
            self.mixer,
            *self.vol_down,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )

        stdout, stderr = await proc.communicate()
        return VolumeProcessState(stdout, stderr)


class DevVolume(Volume):
    def __init__(self, mixer, vol_up, vol_down):
        super().__init__(mixer, vol_up, vol_down)

    async def decrease_volume(self) -> VolumeProcessState:
        result = await super().decrease_volume()
        logger.info("stdout - {}".format(result.stdout))
        logger.info("stderr - {}".format(result.stderr))
        return result

    async def increase_volume(self) -> VolumeProcessState:
        result = await super().increase_volume()
        logger.info("stdout - {}".format(result.stdout))
        logger.info("stderr - {}".format(result.stderr))
        return result
