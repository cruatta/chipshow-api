import os
from typing import List
from pathlib import Path


class PathModel(object):
    def __init__(self, path: str):
        p = Path(path)
        if p.exists() and p == p.absolute():
            self.path = str(p.absolute())
            self.filename = str(p.name)
        else:
            raise ValueError("Invalid Path: {}".format(str(p.absolute())))


class FileSystem(object):
    def __init__(self, base_path: str):
        self.base_path = Path(base_path)

    def filter_tracks(self, filename: str) -> bool:
        filepath: Path = self.base_path / filename
        return (
            filepath.absolute() == filepath
            and filepath.is_file()
            and filename.endswith(".xm")
        )

    def reload_tracks(self) -> List[PathModel]:
        tracks = list(filter(self.filter_tracks, os.listdir(self.base_path)))
        return list(map(self.to_full_path, tracks))

    def to_full_path(self, filename: str) -> PathModel:
        path = self.base_path / filename
        return PathModel(str(path))
