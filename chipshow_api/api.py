from typing import Optional

from starlette.requests import Request
from fastapi.responses import JSONResponse
from chipshow_api.player.exec import UnsupportedOperation
from chipshow_api.track.storage import StorageModel, PaginateModel, TrackIdModel
from chipshow_api.track.fs import FileSystem
from fastapi import FastAPI, HTTPException
from chipshow_api.logger import logger
from chipshow_api.config.version import version
from chipshow_api.config.env import dev
from chipshow_api.config.config import DevConfig, RPIConfig

if dev:
    config = DevConfig()
else:
    config = RPIConfig()


app = FastAPI(version=version, title="Chipshow API")
file_system = FileSystem(config.chiptunes)
storage = StorageModel()
player = config.create_player()
volume = config.create_volume()


@app.exception_handler(ValueError)
async def unicorn_exception_handler(request: Request, exc: ValueError):
    return JSONResponse(status_code=422)


@app.exception_handler(UnsupportedOperation)
async def unicorn_exception_handler(request: Request, exc: UnsupportedOperation):
    return JSONResponse(status_code=400)


@app.on_event("startup")
async def startup_event():
    logger.info("Config: {}".format(config))
    track_paths = file_system.reload_tracks()
    storage.add_tracks(track_paths)


@app.get("/track/{count}")
async def list_tracks(count: int, index: Optional[int] = 0):
    ret = {}
    for k, v in storage.paginate_tracks(PaginateModel(index, count)).items():
        ret[k.id] = v
    return ret


@app.put("/player/{track_id}")
async def play_track(track_id: str):
    maybe_track = storage.get_path(TrackIdModel(track_id))
    if maybe_track:
        state = await player.play(file_system.to_full_path(maybe_track))
        if state.busy:
            raise HTTPException(503)
        else:
            return state.track
    else:
        raise HTTPException(404)


@app.delete("/player")
async def stop():
    return player.stop()


@app.put("/volume/increase")
async def increase_volume():
    await volume.increase_volume()


@app.put("/volume/decrease")
async def decrease_volume():
    await volume.decrease_volume()
