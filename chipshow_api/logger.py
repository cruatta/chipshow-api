import logging
from chipshow_api.config.env import dev

logger = logging.getLogger("chipshow_api")

c_handler = logging.StreamHandler()
c_format = logging.Formatter("{levelname:<9} {message}", style="{")
c_handler.setFormatter(c_format)

logger.addHandler(c_handler)
if dev:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)
