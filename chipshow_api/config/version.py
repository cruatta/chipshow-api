from importlib.metadata import version
from chipshow_api.config.env import dev

if dev:
    version = "{}-dev".format(version("chipshow_api"))
else:
    version = version("chipshow_api")
